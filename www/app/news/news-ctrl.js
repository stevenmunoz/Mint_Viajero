(function () {
    'use strict';

    angular.module('iot').controller('newsCtrl', ['$scope', '$rootScope', '$state', 'newsApi', '$cordovaInAppBrowser', 'config', newsCtrl]);

    function newsCtrl ($scope, $rootScope, $state, newsApi, $cordovaInAppBrowser, config) {

        var vm = this;

         var options = config.inAppOptions;

        vm.getNews = function (forceRefresh) {

            newsApi.getNews(forceRefresh).then(function (data) {
                vm.news = data;    
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }

        vm.browse = function(url) {
            $cordovaInAppBrowser.open(url, '_system', options)
		      .then(function(event) {
		        // success
		      })
		      .catch(function(event) {
		        // error
		      });
		    };

        vm.getNews(false);
    }
})();