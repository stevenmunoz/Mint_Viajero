(function () {
    'use strict';

    angular.module('iot').controller('DiagnosticCtrl', ['$scope', '$rootScope', '$state', '$timeout', '$ionicPopup', 'DSCacheFactory', 'diagnosticApi', DiagnosticCtrl]);

    function DiagnosticCtrl ($scope, $rootScope, $state, $timeout, $ionicPopup, DSCacheFactory, diagnosticApi) {

    	var vm = this;

        vm.url = "router.dtldiagnostic";

        vm.getDiagnostic = function (forceRefresh) {

            diagnosticApi.getDiagnostic(forceRefresh).then(function (data) {
                vm.listDiagnostic = data;    
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }

        $scope.menuTap = function (route) {
          $state.go(route);
        };

        var checkboxes = $("input[type='checkbox']"),
            submitButt = $("#btnDiagnostic");

        checkboxes.click(function() {
            submitButt.attr("disabled", !checkboxes.is(":checked"));
        });

        vm.getDiagnostic(false);
    }
})();