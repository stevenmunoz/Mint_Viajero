(function () {
    'use strict';

    angular.module('iot').controller('FaqsCtrl', ['$scope', '$state', 'DSCacheFactory', 'faqsApi', FaqsCtrl]);

    function FaqsCtrl($scope, $state, DSCacheFactory, faqsApi) {

        self.faqsDetailDataCache = DSCacheFactory.get("faqsDetailDataCache");

        var vm = this,
            question = "question",
            answer = "answer";

        vm.loadFaqsList = function (forceRefresh) {

            faqsApi.getFaqs(forceRefresh).then(function (data) {
                vm.faqs = data.preguntasFrecuentes;    
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }
        
        vm.loadFaqsList(false);
        vm.url = "router.faqs-detail";

        $scope.toggleGroup = function(group) {
          if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
          } else {
            $scope.shownGroup = group;
          }
        };

        $scope.isGroupShown = function(group) {
          return $scope.shownGroup === group;
        };

        $scope.menuTap = function (route, quest, ans) {
          self.faqsDetailDataCache.put(question, quest);
          self.faqsDetailDataCache.put(answer, ans);
          $state.go(route);
        };
        
    };
})();