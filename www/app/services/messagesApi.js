(function() {
	'use strict';

	angular.module('iot').factory('messagesApi', ['$http', '$q', '$ionicLoading', 'DSCacheFactory', 'config', '$timeout', messagesApi]);
		
	function messagesApi($http, $q, $ionicLoading, DSCacheFactory, config, $timeout) {

		var cacheKeyId = "id";
		self.loginDataCache = DSCacheFactory.get("loginDataCache");
		self.userDataCache = DSCacheFactory.get("userDataCache");
		
		/*
			Description:
			Return all inbox messages by user
		*/
     	function getInboxMessages (forceRefresh) {
			
			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

			var deferred = $q.defer(),
				loginData = self.loginDataCache;

			$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

            var data = {
			  	"receptor": self.userDataCache.get(cacheKeyId)
			 };

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.inboxUrl,
				headers: {'Content-Type': 'application/json'},
				data: data
			};

			$http(req).success (function (data) {
					$ionicLoading.hide();
					deferred.resolve(data);
				})
				.error(function () {
					console.log("Error making http call on inbox messages");
					$ionicLoading.hide();
					deferred.reject();	
			});
			
			return deferred.promise;
		};

		/*
			Description:
			Return all outbox messages by user
		*/
		function getOutboxMessages (forceRefresh) {
			
			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

			var deferred = $q.defer(),
				cacheKey = "outbox",
				loginData = self.loginDataCache;

			$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

            var data = {
			  	"emisor": self.userDataCache.get(cacheKeyId)
			 };

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.outboxUrl,
				headers: {'Content-Type': 'application/json'},
				data: data
			};

			$http(req).success (function (data) {
				$ionicLoading.hide();
				console.log("receive outbox msgs from http");
				deferred.resolve(data);
			}).error(function () {
				console.log("Error making http call on outbox msg");
				$ionicLoading.hide();
				deferred.reject();	
					
			});
			
			return deferred.promise;
		};

		/*
			Description:
			Return detail message
		*/
		function getMessageDetail(id) 
		{

			var deferred = $q.defer(),
			loginData = self.loginDataCache;

			$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

            var data = {
			  	 "id": id,
 				 "receptor": self.userDataCache.get(cacheKeyId)
			 };

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.getMessageUrl,
				headers: {'Content-Type': 'application/json'},
				data: data
			};

			$http(req).success (function (data) {
					$ionicLoading.hide();
					console.log("receive detail msgs from http");
					deferred.resolve(data);

				})
				.error(function () {
					console.log("Error making http call on detail message");
					$ionicLoading.hide();
					deferred.reject();	
					
			});
			
			return deferred.promise;
		};

		function sendMessage(dataMessage)
		{
			var deferred = $q.defer(),
			loginData = self.loginDataCache;

			$ionicLoading.show({
              	template: 'Enviando Mensaje ...'
            });

            var data = {
 				 "usuarioEmisorId": 0, 
 				 "codaEmisor": self.userDataCache.get(cacheKeyId),
				 "usuarioReceptorId": 0,
				 "codaReceptor": dataMessage.receptor.ID,
				 "titulo": dataMessage.title,
				 "contenido": dataMessage.content,
				 "fueLeido": false
			 };

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.sendMessageUrl,
				headers: {'Content-Type': 'application/json'},
				data: data
			};

			$http(req).success (function (data) {

					if (!data.success) {
						$ionicLoading.show({
                                template: 'Ha ocurrido un error enviando el mensaje, intente en unos minutos.'
                            });
                            $timeout( function() {
                                $ionicLoading.hide();
                                vm.dataMessage = "";
                                deferred.reject();	
                         }, 2000);
					}
					 else {
						$ionicLoading.hide();
						console.log("sent msgs from sender");
						deferred.resolve(data);
					}
				})
				.error(function () {
					console.log("Error sending message");
					$ionicLoading.hide();
					deferred.reject();	
					
			});
			
			return deferred.promise;
		}

		function getUnreadMessages () {

			var deferred = $q.defer();
				
			var data = {
			  	"usuario": self.userDataCache.get(cacheKeyId)
			 };

			var req = {
				method: 'POST',
				url: config.backendEndpoint + config.getUnreadMessagesUrl,
				headers: {'Content-Type': 'application/json'},
				data: data
			};

			$http(req)
				.success(function (data) {
					deferred.resolve(data);
				})
				.error(function () {
					console.log("Error making http unread messages call on service");
					deferred.reject();	
				});
			
			return deferred.promise;
		}

		//reveal module pattern 	
		return {
			getInboxMessages: getInboxMessages,
			getOutboxMessages: getOutboxMessages,
			getMessageDetail: getMessageDetail,
			sendMessage: sendMessage,
			getUnreadMessages: getUnreadMessages
		};
	};

})();