(function(){
	'use strict';

	angular.module('iot').factory('casesApi', ['$http', '$q', 'DSCacheFactory', '$ionicLoading', 'config', casesApi]);

	function casesApi ($http, $q, DSCacheFactory, $ionicLoading, config) {

		self.casesDataCache = DSCacheFactory.get("casesDataCache");
		
		self.casesDataCache.setOptions({
            onExpire: function (key, value) {
                getCases(true)
                    .then(function () {
                        console.log("Cases data Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting cases data. Putting expired item back in the cache.", new Date());
                        self.casesDataCache.put(key, value);
                    });
            }
        });

        function getCases (forceRefresh) {

        	$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

        	var deferred = $q.defer(),
        		requestUrl = config.testEndpoint,
				cacheKey = "cases",
				casesData = null;

			var req = {
				method: 'GET',
				url: requestUrl + config.getCases
			};
			
				
			//Request for cases

			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

	        if (!forceRefresh) {

				casesData = self.casesDataCache.get(cacheKey);
			}

			if(casesData) {
				$ionicLoading.hide();
				console.log("Found cases data inside cache");
				deferred.resolve(casesData);

			} else {
					
				$http(req).success(function (data) {
					$ionicLoading.hide();
					self.casesDataCache.put(cacheKey, data);
					deferred.resolve(data);

				}).error(function (data) {
						
					$ionicLoading.hide();
					casesData = self.casesDataCache.get(cacheKey);

					if(casesData) {
						deferred.resolve(casesData);
					} else {
						deferred.reject();	
					}
				});
			}

			return deferred.promise;
		}

        return {
        	getCases: getCases
        };
	}

})();