(function() {
	'use strict';

	angular.module('iot').factory('pointsApi', ['$http', '$q', '$ionicLoading', 'DSCacheFactory', 'config', pointsApi]);
		
	function pointsApi($http, $q, $ionicLoading, DSCacheFactory, config) {

		self.servicePointsDataCache = DSCacheFactory.get("servicePointsDataCache");
		self.loginDataCache = DSCacheFactory.get("loginDataCache");
		
		/*
			Description:
			Cache on expire configuration, reload data again if fails, put back in cache latest key value
		*/
		self.servicePointsDataCache.setOptions({
            onExpire: function (key, value) {
                getPoints()
                    .then(function () {
                        console.log("Points Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting points data. Putting expired item back in the cache.", new Date());
                        self.servicePointsDataCache.put(key, value);
                    });
            }
        });

		/*
			Description:
			Retorn all points from service
		*/
     	function getPoints () {
			
			var deferred = $q.defer(),
				cacheKey = "points",
				pointsData = self.servicePointsDataCache.get(cacheKey),
				loginData = self.loginDataCache;

			//is there points inside cache ?
			if(pointsData) {
				console.log("Found points inside cache");
				deferred.resolve(pointsData);
			} else {
				
				$ionicLoading.show({
              		template: 'Verificando Riesgos Cercanos ...'
            	});

				var req = {
					method: 'GET',
					url: config.testEndpoint + config.getPoints,
					params: {}
				};

				$http(req).success (function (data) {
						$ionicLoading.hide();
						console.log("receive points from http");
						self.servicePointsDataCache.put(cacheKey, data);
						deferred.resolve(data);
					})
					.error(function () {
						console.log("Error making http call on points service");
						$ionicLoading.hide();

						pointsData = self.servicePointsDataCache.get(cacheKey);

						if(pointsData) {
							console.log("Found points data inside cache");
							deferred.resolve(pointsData);
						} else {
							deferred.reject();	
						}
					});
			}

			return deferred.promise;
		};

		/*
			Description:
			Retorn nearest point
		
			Params:
			@latitude: actual point latitude
			@longitude: actual point longitude
		*/
		function getNearestPoint (latitude, longitude) {
			
			var deferred = $q.defer(),
				loginData = self.loginDataCache;

			//is there points inside cache ?
				
			$ionicLoading.show({
              	template: 'Cargando ACR Cercano ...'
            });

			var req = {
				method: 'GET',
				url: config.acrEndPoint + config.getNearestPoint,
				headers: {'Authorization': loginData.get(config.tokenType)+' '+loginData.get(config.tokenKey)},
				params: {longitud:longitude, latitud:latitude}
			};

			$http(req).success (function (data) {
				$ionicLoading.hide();
				console.log("receive nearest point");
				deferred.resolve(data);
			})
			.error(function () {
				console.log("Error receiving nearest point");
				$ionicLoading.hide();
				deferred.reject();	
			});
			

			return deferred.promise;
		};


		//reveal module pattern 	
		return {
			getPoints: getPoints,
			getNearestPoint: getNearestPoint 
		};
	};

})();