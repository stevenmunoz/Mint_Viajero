(function(){
	'use strict';

	angular.module('iot').factory('countriesApi', ['$http', '$q', 'DSCacheFactory', 'config', countriesApi]);

	function countriesApi ($http, $q, DSCacheFactory, config){
		
		self.statesDataCache = DSCacheFactory.get("statesDataCache");
		
		function getStates () {
			
			var deferred = $q.defer(),
				cacheKey = "states",
				statesData = self.statesDataCache.get(cacheKey);

			if(statesData) {
				deferred.resolve(statesData);
			} 
			else {

				var req = {
					method: 'GET',
					url: config.countriesEndpoint + config.getStates
				};

				$http(req)
					.success(function (data) {
						self.statesDataCache.put(cacheKey, data);
						deferred.resolve(data);
					})
					.error(function () {
						statesData = self.statesDataCache.get(cacheKey);
						if(statesData) {
							deferred.resolve(statesData);
						} else {
							deferred.reject();	
						}
					});
			}

			return deferred.promise;
		}
		
		function getCities (stateId) {
			
			var deferred = $q.defer();
			
			var req = {
				method: 'GET',
				url: config.countriesEndpoint + config.getCities + stateId
			};

			$http(req)
				.success(function (data) {
					deferred.resolve(data);
				})
				.error(function () {
					deferred.reject();	
				});
			
			return deferred.promise;
		}


		return {
			getStates: getStates,
			getCities: getCities
		};

	}
})();