(function () {
	'use strict';

	angular.module('iot').factory('introApi', ['$http', '$q', 'DSCacheFactory', 'config', '$ionicLoading', introApi]);

	function introApi ($http, $q, DSCacheFactory, config, $ionicLoading) {

		//TODO: REVIEW CACHE 
		//Specially if loginData is assigned in success callback, and it was erased ?
		//Optimize in one method, just return a promise and receive, url, headers, params as
		//function args

		self.loginDataCache = DSCacheFactory.get("loginDataCache");

		//First automatic login to get api token
		function autoLoginUser (userName, password) {

			var defered = $q.defer(),
				loginData = self.loginDataCache;

			var req = {
				method: 'POST',
				url: config.acrEndPoint + 'Token',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
				    var str = [];
				    for(var p in obj)
				    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				    return str.join("&");
				 },
				data: {username: userName, password: password, grant_type: 'password'}
			};

			$http(req).success(function (data) {
				self.loginDataCache.put(config.tokenKey, data.access_token);
				self.loginDataCache.put(config.tokenType, data.token_type);
				loginData = self.loginDataCache;
				defered.resolve(data);

				}).error(function (data, status, headers, config) {
					console.log("Autologin error " + data);
					defered.reject();
				});
		
			return defered.promise;
		}

		//User manual login
		function loginUser (userName, password, role) {

			self.loginDataCache = DSCacheFactory.get("loginDataCache");

			var defered = $q.defer(),
				requestUrl = config.acrEndPoint,
				loginData = self.loginDataCache;

			if (role === "Ciudadano") {
				requestUrl += config.validateCitizen;
			} else if (role === "Ppr") {
				requestUrl += config.validatePpr;
			} else if (role === "Reintegrador") {
				requestUrl += config.validateProfessionalUrl;
			}

			var req = {
				method: 'GET',
				url: requestUrl,
				headers: {'Authorization': loginData.get(config.tokenType)+' '+loginData.get(config.tokenKey)},
				params: {username: userName, password: password}
			};
			
			$http(req).success(function (data) {
				defered.resolve(data);
			}).error(function (data) {
				defered.reject();
			});
				
			return defered.promise;
		}

		function getUserInfo (userName, role) {

			var defered = $q.defer(),
				requestUrl = config.acrEndPoint,
				parameters = {},
				loginData = self.loginDataCache;

			if (role === "Ciudadano") {
				//requestUrl += config.getProfessionalInfo;
				//parameters = {username: userName};
			} else if (role === "Ppr") {
				requestUrl += config.getPPrInfo;
				parameters = {coda: userName};
			} else if (role === "Reintegrador") {
				requestUrl += config.getProfessionalInfo;
				parameters = {username: userName};
			}

			var req = {
				method: 'GET',
				url: requestUrl,
				headers: {'Authorization': loginData.get(config.tokenType)+' '+loginData.get(config.tokenKey)},
				params: parameters
			};
			
			$http(req).success(function (data) {
				defered.resolve(data);
			}).error(function (data) {
				defered.reject();
			});
				
			return defered.promise;
		}

		function getRegisterForm (userType) {

			$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

			var defered = $q.defer(),
				requestUrl = config.acrEndPoint,
				parameters = {},
				loginData = self.loginDataCache;

			if (userType === "Ciudadano") {
				requestUrl += config.getUserRegisterForm;
			} else if (userType === "Ppr") {
				requestUrl += config.getPPRRegisterForm;
			} 

			var req = {
				method: 'GET',
				url: requestUrl,
				headers: {'Authorization': loginData.get(config.tokenType)+' '+loginData.get(config.tokenKey)},
				params: {}
			};
			
			$http(req).success(function (data) {
				$ionicLoading.hide();
				defered.resolve(JSON.parse(data));
			}).error(function (data) {
				$ionicLoading.hide();
				defered.reject();
			});
				
			return defered.promise;
		}

		function logout () {

			var defered = $q.defer(),
				loginData = self.loginDataCache;

			$ionicLoading.show({
              	template: 'Saliendo ...'
            });

			var req = {
				method: 'POST',
				url: config.acrEndPoint + config.logoutUrl,
				headers: { 'Authorization': loginData.get(config.tokenType)+' '+loginData.get(config.tokenKey)}
			};
						
			$http(req).success(function (data) {
				$ionicLoading.hide();
				defered.resolve(JSON.parse(data));
				
			}).error(function (data) {
				$ionicLoading.hide();
				defered.reject();
			});
				
			return defered.promise;
		}

		/*
			Description:
			Return all inbox messages by user
		*/
     	function getDimensions () {
			var deferred = $q.defer(),
				loginData = self.loginDataCache;

			$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

			var req = {
				method: 'GET',
				url: config.backendEndpoint + config.getDimensionsUrl,
				headers: {'Content-Type': 'application/json'},
			};

			$http(req).success (function (data) {
					$ionicLoading.hide();
					deferred.resolve(data);
				})
				.error(function () {
					console.log("Error making http call on inbox messages");
					$ionicLoading.hide();
					deferred.reject();	
			});
			
			return deferred.promise;
		};

		return {
			loginUser: loginUser,
			autoLoginUser: autoLoginUser,
			getUserInfo: getUserInfo,
			getRegisterForm: getRegisterForm,
			logout: logout,
			getDimensions: getDimensions
		};
	};

})();