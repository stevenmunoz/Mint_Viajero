(function(){
	'use strict';

	angular.module('iot').factory('diagnosticApi', ['$http', '$q', 'DSCacheFactory', '$ionicLoading', 'config', diagnosticApi]);

	function diagnosticApi ($http, $q, DSCacheFactory, $ionicLoading, config) {

		self.diagnosticDataCache = DSCacheFactory.get("diagnosticDataCache");
		
		self.diagnosticDataCache.setOptions({
            onExpire: function (key, value) {
                getDiagnostic(true)
                    .then(function () {
                        console.log("Diagnostic data Cache was automatically refreshed.", new Date());
                    }, function () {
                        console.log("Error getting diagnostic data. Putting expired item back in the cache.", new Date());
                        self.diagnosticDataCache.put(key, value);
                    });
            }
        });

         function getDiagnostic (forceRefresh) {

        	$ionicLoading.show({
              	template: 'Cargando Información ...'
            });

        	var deferred = $q.defer(),
        		requestUrl = config.testEndpoint,
				cacheKey = "diagnostic",
				diagnosticData = null;

			var req = {
				method: 'GET',
				url: requestUrl + config.getDiagnostic
			};
			
			//Request for news

			if (typeof forceRefresh === "undefined") { forceRefresh = false; }

		     	if (!forceRefresh) {
					diagnosticData = self.diagnosticDataCache.get(cacheKey);
				}

				if (diagnosticData) {
					$ionicLoading.hide();
					console.log("Found diagnostic data inside cache");
					deferred.resolve(diagnosticData);

				} else {
						
					$ionicLoading.show({
		              	template: 'Cargando Información ...'
		            });

					$http(req).success(function (data) {
						$ionicLoading.hide();
						self.diagnosticDataCache.put(cacheKey, data);
						deferred.resolve(data);

					}).error(function (data) {
							
						$ionicLoading.hide();
						diagnosticData = self.diagnosticDataCache.get(cacheKey);

						if(diagnosticData) {
							deferred.resolve(diagnosticData);
						} else {
							deferred.reject();	
						}
					});
				}

				return deferred.promise;
	    }

        return {
        	getDiagnostic: getDiagnostic,
        };
	}

})();