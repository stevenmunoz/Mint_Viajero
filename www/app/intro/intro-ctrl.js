 (function () {
    'use strict';

    angular.module('iot').controller('IntroCtrl', ['$scope', '$rootScope', '$state', '$ionicSlideBoxDelegate', '$timeout', '$ionicLoading', '$ionicPopup', '$cordovaInAppBrowser', 'DSCacheFactory', 'introApi', 'userApi', 'config', IntroCtrl]);
        
        function IntroCtrl ($scope, $rootScope, $state, $ionicSlideBoxDelegate, $timeout, $ionicLoading, $ionicPopup, $cordovaInAppBrowser, DSCacheFactory, introApi, userApi, config) {

            angular.element(document).ready(function () {
                    $('map').imageMapResize();
                    /*$('area').on('click', function() {
                        alert($(this).attr('alt') + ' clicked');
                    });*/
                });

            self.userDataCache = DSCacheFactory.get("userDataCache");

            var vm = this,
                cacheKeyRole = "role",
                cacheKeyRoleId = "roleId",
                cacheKeyName = "name",
                role = "",
                cacheKeyId = "id",
                cacheKeyPpr = "pprId";

            var options = config.inAppOptions;

            vm.autoLogin = function () {

                introApi.autoLoginUser(config.apiUserName, config.apiUserPass).then(function (data){
                    vm.dataLogin = data;
                }, function(reason) {
                    console.log("An error has ocurred in Auto Login " + reason);
                });
            };

            vm.saveRole = function(roleId) {

                switch (roleId) {
                  case 0:
                    role = "Reintegrador";
                    break;
                  case 1:
                    role = "Ppr";
                    break;
                  case 2:
                    role = "Ciudadano";
                    break;
                }   

                vm.role = roleId;
                self.userDataCache.put(cacheKeyRole, role);
                self.userDataCache.put(cacheKeyRoleId, roleId);

                $ionicSlideBoxDelegate.next();

            };

            vm.login = function() {

                $ionicLoading.show({
                  template: 'Validando Datos...'
                });

                introApi.loginUser(vm.username, vm.password, role).then(function (data){
                    vm.dataLogin = data;
                    if (data === "true") {

                        introApi.getUserInfo(vm.username, role).then(function(data){
                            
                            if (role === "Ppr") {
                                //Almaceno el id del ppr para utilizarlo en el plan de trabajo activo
                                self.userDataCache.put(cacheKeyPpr, data.ID);
                                
                                self.userDataCache.put(cacheKeyId, data.CODA);
                                self.userDataCache.put(cacheKeyName, data.PrimerNombre + ' ' + data.SegundoNombre + ' ' + data.PrimerApellido + ' ' + data.SegundoApellido);
                            } else if (role == "Reintegrador") {
                               self.userDataCache.put(cacheKeyId, data.ID);
                               self.userDataCache.put(cacheKeyName, data.NombreCompleto);
                            }

                            $state.go("router.dimensions");
                            //save user on backend
                            userApi.saveUser(self.userDataCache.get(cacheKeyId), self.userDataCache.get(cacheKeyName), self.userDataCache.get(cacheKeyRole)).then(function (data){
                                console.log("User saved on backend -> " + data.success);  
                            }, function(reason) {
                                console.log("Error saving user on backend " + reason);
                            });
                            
                        }, function(reason) {
                            $ionicLoading.show({
                                template: 'Ha ocurrido un error obteniendo los datos del usuario, intente en unos minutos.'
                            });
                            $timeout( function() {
                                $ionicLoading.hide();
                            }, 1000);
                        });

                        $ionicLoading.show({
                          template: 'Correcto !'
                        });
                        $timeout( function() {
                            $ionicLoading.hide();
                            //$ionicSlideBoxDelegate.next();
                        }, 500); 

                    } else {

                        $ionicLoading.show({
                            template: 'Los datos ingresados son incorrectos !'
                        });
                        $timeout( function() {
                            $ionicLoading.hide();
                        }, 1000);
                    }
                    
                }, function(reason) {
                    $ionicLoading.show({
                          template: 'Ha ocurrido un error, intente en unos minutos.'
                    });
                    $timeout( function() {
                        $ionicLoading.hide();
                    }, 1000);
                });

            };

            vm.nextSlide = function() {
                $ionicSlideBoxDelegate.next();
            };

            vm.prevSlide = function() {
                $ionicSlideBoxDelegate.previous();
            };

            //Disable intro slide
            vm.initSlider = function() {
               $ionicSlideBoxDelegate.enableSlide(false);

               //Set user type from cache
               if (self.userDataCache.get(cacheKeyRoleId) != "") {

                    
               }
            };

            vm.showRegister = function() {
                $scope.data = {}
                var myPopup = $ionicPopup.show({
                    template: '<input type="email" ng-model="data.email">',
                    title: 'Ingresa Tu Correo Electrónico',
                    subTitle: 'Serás notificado una vez aprobado',
                    scope: $scope,
                    buttons: [
                        { text: 'Cancelar' },
                        {
                         text: '<b>Enviar</b>',
                         type: 'button-balanced',
                         onTap: function(e) {
                           if (!$scope.data.email) {
                             e.preventDefault();
                           } else {
                             return $scope.data.email;
                           }
                         }
                        },
                    ]
                });
            };

            vm.openRegisterForm = function (userType) {

                introApi.getRegisterForm(userType).then(function (data){
                    vm.browse(data);
                }, function(reason) {
                    console.log("An error has ocurred retrieving register form url " + reason);
                });
            }

            vm.browse = function(url) {
            $cordovaInAppBrowser.open(url, '_system', options)
              .then(function(event) {
                // success
              })
              .catch(function(event) {
                // error
              });
            };

            //Get access token for user
            vm.autoLogin();
        }
 })();