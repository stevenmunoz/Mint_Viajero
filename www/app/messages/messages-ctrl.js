(function () {
    'use strict';

    angular.module('iot').controller('messagesCtrl', ['$scope', 'messagesApi', messagesCtrl]);

    function messagesCtrl($scope, messagesApi) {
        var vm = this;

        vm.loadInboxMessages = function (forceRefresh) {

            messagesApi.getInboxMessages(forceRefresh).then(function (data) {
                vm.inboxMessages = data.result.mensajes;    
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }

         vm.loadOutboxMessages = function (forceRefresh) {

            messagesApi.getOutboxMessages(forceRefresh).then(function (data) {
                vm.outboxMessages = data.result.mensajes;    
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });    
        }
    
        vm.loadInboxMessages(false);
        vm.loadOutboxMessages(false);
    };
})();