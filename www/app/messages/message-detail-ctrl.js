(function () {
    'use strict';

    angular.module('iot').controller('messageDetailCtrl', ['$scope', '$rootScope' ,'$stateParams', 'messagesApi', messageDetailCtrl]);

    function messageDetailCtrl($scope, $rootScope, $stateParams, messagesApi) {
        var vm = this;

        vm.loadDetail = function (){
            messagesApi.getMessageDetail(Number($stateParams.id)).then(function (data) {
                
                vm.detailMessage = {
                    tittle: data.result.titulo,
                    content: data.result.contenido,
                    emisor: $stateParams.emisor
                }

                //Reload unread messages
                messagesApi.getUnreadMessages().then(function (data){
                    $rootScope.messages = data.result.mensajes;
                });
            
            });    
        };
        vm.loadDetail();
    };
})();