(function () {
    'use strict';

    angular.module('iot').controller('HomeCtrl', ['$scope', '$rootScope', '$state', 'DSCacheFactory', 'messagesApi', HomeCtrl]);

    function HomeCtrl ($scope, $rootScope, $state, DSCacheFactory, messagesApi) {

    	self.userDataCache = DSCacheFactory.get("userDataCache");
    	var role = userDataCache.get("role");
    	var vm = this;

          //Get unread messages
        function loadUnreadMessag () {

            messagesApi.getUnreadMessages().then(function (data){
                $rootScope.messages = data.result.mensajes;
            });
        }              

        vm.menuTap = function (route) {
            $state.go(route);
        };

        //loadUnreadMessag();
    }
})();